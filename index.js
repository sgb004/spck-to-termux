const ABSPATH = process.cwd();
const fs = require('fs');
const chokidar = require('chokidar');

const log = console.log.bind(console);

class Watcher {
	from;
	to;
	file;
	ignored;
	bidirectional = false;

	constructor() {
		fs.readFile(ABSPATH + '/stt.conf.json', this.conf.bind(this));
	}

	conf(err, data) {
		if (err) throw err;
		let conf = JSON.parse(data);
		if (!conf.from) throw 'from parameter is undefined';
		this.from = conf.from + '/';
		this.from = this.from.replace(/\/\//g, '/');
		this.to = conf.to || ABSPATH;
		this.to = this.to + '/';
		this.to = this.to.replace(/\/\//g, '/');

		if (!conf.from) throw 'ignored parameter is undefined';
		this.ignored = conf.ignored;

		this.bidirectional = conf.bidirectional;
		this.watcherInit();
	}

	watcherInit() {
		const watcher = chokidar.watch(this.from, {
			ignored: this.ignored,
			persistent: true,
		});

		watcher
			.on('add', this.watcherChange.bind(this))
			.on('change', this.watcherChange.bind(this))
			.on('unlink', this.watcherUnlink.bind(this))
			.on('addDir', this.watcherAddDir.bind(this))
			.on('unlinkDir', this.watcherUnlinkDir.bind(this));
	}

	watcherChange(path) {
		log(`File ${path} has been changed`);
		this.file = path.replace(this.from, '');
		fs.copyFile(path, this.to + this.file, this.copyFile.bind(this));
	}

	watcherUnlink(path) {
		log(`File ${path} has been deleted`);
		this.file = path.replace(this.from, '');
		fs.unlink(this.to + this.file, this.unlink.bind(this));
	}

	watcherAddDir(path) {
		log(`Dir ${path} has been added`);
		const stats = fs.statSync(this.from);
		this.file = path.replace(this.from, '');
		const fromFile = this.to + this.file;

		if (stats.isDirectory()) {
			if (!fs.existsSync(fromFile)) {
				fs.mkdirSync(fromFile);
			}
		}
	}

	watcherUnlinkDir(path) {
		log(`Dir ${path} has been deleted`);
		this.file = path.replace(this.from, '');
		fs.rmdir(
			this.to + this.file,
			{ recursive: true },
			this.unlink.bind(this)
		);
	}

	copyFile(err) {
		if (err) throw err;
		log(this.file + ' was copied to ' + this.to + this.file);
	}

	unlink(err) {
		//if (err) throw err;
		log(this.file + ' was deleted to ' + this.to + this.file);
	}
}
new Watcher();
