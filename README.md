# SPCK to Termux

This package copy files from a spck editor directory to a termux directory.

## Dependencies

```
chokidar
```

## Installation

```sh
npm install spck-to-termux
```

## Usage

Add this JSON in a file with the name of stt.conf.json in the directory of your project

```json
{
	"from": "",
	"to": "",
	"ignored": ""
}
```

### Where:

-   from: Directory from where files are copied.
-   to: (optional) Directory where the files will be pasted, if it is not added, the directory of your project will be used.
-   ignored: files to ignore, check chokidar documentation to know how to ignore files [https://www.npmjs.com/package/chokidar](https://www.npmjs.com/package/chokidar).

Add in your scripts list of your package.json:

```json
"stt": "node node_modules/spck-to-termux"
```

And run with:

```sh
npm run stt
```
